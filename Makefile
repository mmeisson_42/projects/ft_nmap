
NAME			= ft_nmap

CC				= gcc

CFLAGS			= -MD -Wall -Werror -Wextra -funroll-loops -g

VPATH			= ./srcs:./gnl:./srcs/command_args/:./srcs/helpers/:./srcs/scans/:./srcs/listener/

SRCS			= main.c arg_parse.c arg_handlers.c resolve_host.c
SRCS			+= get_host_by_addr.c register_host.c get_next_line.c
SRCS			+= get_scan_code.c print_helper.c arg_manage.c syn_scan.c
SRCS			+= ack_scan.c send_raw_packet.c udp_scan.c checksum.c
SRCS			+= fin_scan.c xmas_scan.c null_scan.c print_network.c
SRCS			+= init_responses.c listener_entry.c
SRCS			+= handle_network.c handle_tcp.c handle_udp.c scans.c
SRCS			+= print_configuration.c print_resume.c state_to_str.c
SRCS			+= scan_idx_to_str.c scan_idx_to_mask.c compute_latency.c
SRCS			+= get_service_name_by_port.c craft_filter.c get_mac_addr.c
SRCS			+= get_interface_address.c inet_mac_pton.c get_interface.c
SRCS			+= hosts_eraser.c get_host_by_addr.c con_scan.c

SRCS			+= hexdump.c

INCS_PATHS		= ./incs/ ./srcs/ ./libft/incs/ ./gnl/ ./srcs/command_args/ ./srcs/listener
INCS_PATHS		+= ./srcs/helpers/ ./srcs/scans/
INCS			= $(addprefix -I,$(INCS_PATHS))

OBJS_PATH		= ./.objs/
OBJS_NAME		= $(SRCS:.c=.o)
OBJS			= $(addprefix $(OBJS_PATH), $(OBJS_NAME))


DEPS			= $(OBJS:.o=.d)

LIB_PATHS		= ./libft/
LIBS			= $(addprefix -L,$(LIB_PATHS))

LDFLAGS			= $(LIBS) -lft -lpcap -lpthread



all: $(NAME)


$(NAME): $(OBJS)
	@$(foreach PATHS, $(LIB_PATHS),\
		echo "# # # # # #\n#";\
		echo '# \033[31m' Compiling $(PATHS) '\033[0m';\
		echo "#\n# # # # # #";\
		make -j32 -C $(PATHS);\
	)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJS_PATH)%.o: $(SRCS_PATHS)%.c Makefile
	@mkdir -p $(OBJS_PATH)
	$(CC) $(CFLAGS) $(INCS) -o $@ -c $<

self_clean:
	rm -rf $(OBJS_PATH)

clean: self_clean
	@$(foreach PATHS, $(LIB_PATHS), make -C $(PATHS) clean;)

self_fclean:
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

fclean: self_fclean
	@$(foreach PATHS, $(LIB_PATHS), make -C $(PATHS) fclean;)

re:
	make fclean
	make -j32 all

test:
	make -C tests
	./tests/ft_nmap_tests

-include $(DEPS)
