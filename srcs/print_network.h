#ifndef PRINT_NETWORK_H
# define PRINT_NETWORK_H

#define MAC_FMT "%02x:%02x:%02x:%02x:%02x:%02x"
#define PRINT_MAC(addr) printf(MAC_FMT, addr[0], addr[1], addr[2], addr[3], addr[4], addr[5])

void	print_network(u_char *network);

#endif
