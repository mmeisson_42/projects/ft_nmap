
#include "helpers.h"
#include "listeners.h"
#include "scans.h"
#include "hash_map.h"

volatile int		signaled = 0;
pcap_t				*handle = NULL;

static int	init_pcap(char *dev, char *str_filter)
{
	char				errbuff[512];
	struct bpf_program	bp = { 0 }; // Where the compiled filter will be stored
	bpf_u_int32			netp = 0, maskp = 0;

	if (dev == NULL)
	{
		dev = pcap_lookupdev(errbuff);
	}
	if (dev == NULL)
	{
		dprintf(2, "err : %s\n", errbuff);
		return -1;
	}
	if (pcap_lookupnet(dev, &netp, &maskp, errbuff) == -1)
	{
		dprintf(2, "err : %s\n", errbuff);
		return -1;
	}
	handle = pcap_open_live(dev, 1000, 0, -1, errbuff);
	if (handle == NULL)
	{
		dprintf(2, "err : %s\n", errbuff);
		return -1;
	}
	pcap_compile(handle, &bp, str_filter, 0, 0); // Create the filter
	pcap_setfilter(handle, &bp); // Apply it
	return 0;
}

static void		handle_alarm(__attribute__((unused))int sig)
{
	pcap_breakloop(handle);
	signaled = 1;
}

static int		handle_packets(int (*callback)(struct pcap_pkthdr *, u_char *, void *),
					void *param)
{
	u_char				*packet;
	struct pcap_pkthdr	header = { 0 };
	struct sigaction	act = {
		.sa_handler = handle_alarm,
	};

	sigaction(SIGALRM, &act, NULL);
	while (signaled == 0)
	{
 		packet = (u_char *)pcap_next(handle, &header);
		if (packet != NULL)
		{
			int res = callback(&header, packet, param);
			if (res < 0)
			{
				break ;
			}
		}
	}
	pcap_close(handle);
	return 0;
}

void		listener_entry(struct s_hashmap *map, s_options *opts)
{
	char		*filter;
	pthread_t	scanners[sizeof(pthread_t) * opts->threads_number];
	void		*args[] = { map, opts };

	filter = craft_filter(opts);
	if ((opts->mask_scans & REQ_SCAN_ROOT) && filter != NULL)
	{
		if (init_pcap("any", filter) == -1)
		{
			//TODO: clean everything.
			dprintf(2, "Fatal: Couldn't init pcap\n");
			exit(1);
		}
		// Launching scanning threads
	}
	for (unsigned int index = 0; index < opts->threads_number + 1; index++)
	{
		pthread_create(&scanners[index], 0, launch_scans , args);
	}
	puts("Scanning...");
	if ((opts->mask_scans & REQ_SCAN_ROOT))
		handle_packets(handle_network, map);
	for (unsigned int index = 0; index < opts->threads_number + 1; index++)
	{
		pthread_join(scanners[index], NULL);
	}
	free(filter);
}
