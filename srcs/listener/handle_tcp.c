
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

#include "ft_nmap.h"
#include "helpers.h"
#include "listeners.h"

static int		get_scan_type(unsigned short dport)
{
	for (size_t i = 0; i < SCAN_TYPES; i++)
	{
		if (g_port_table.ports[i] == dport)
			return i;
	}
	return -1;
}

static void		register_tcp(
	struct ethhdr *eth,
	struct iphdr *ip,
	struct tcphdr *tcp,
	s_response *current_response
)
{
	const struct {char options; e_service_state state;}		map_scan_state[][5] = {
		[ SCAN_SYN ] = {
			{.options = TH_SYN + TH_ACK, .state = OPEN},
			{.options = TH_SYN, .state = OPEN},
			{.options = TH_ACK, .state = OPEN},
			{.options = TH_RST, .state = CLOSE},
			{.options = TH_ACK + TH_RST, .state = CLOSE},
		},
		[ SCAN_ACK ] = {
			{.options = TH_SYN + TH_ACK, .state = UNFILTERED}, // ?
			{.options = TH_SYN, .state = UNFILTERED}, // ?
			{.options = TH_RST, .state = UNFILTERED},
		},
		[ SCAN_FIN ] = {
			{.options = TH_RST, .state = CLOSE},
			{.options = TH_ACK + TH_RST, .state = CLOSE},
		},
		[ SCAN_NULL ] = {
			{.options = TH_RST, .state = CLOSE},
			{.options = TH_ACK + TH_RST, .state = CLOSE},
		},
		[ SCAN_XMAS ] = {
			{.options = TH_RST, .state = CLOSE},
			{.options = TH_ACK + TH_RST, .state = CLOSE},
		},
	};
	int		scan_type = get_scan_type(ntohs(tcp->th_dport));

	if (
		(scan_type == SCAN_SYN)
		|| (scan_type == SCAN_ACK)
		|| (scan_type == SCAN_FIN)
		|| (scan_type == SCAN_NULL)
		|| (scan_type == SCAN_XMAS)
	)
	{
		FOR_ARRAY(map_scan_state[scan_type], i)
		{
			if (map_scan_state[scan_type][i].options == tcp->th_flags)
			{
				current_response->scan_state[scan_type] = map_scan_state[scan_type][i].state;
				break ;
			}
		}
	}
	(void)eth;
	(void)ip;

}

void		handle_tcp(
	struct ethhdr *eth,
	struct iphdr *ip,
	struct tcphdr *tcp,
	s_response *responses
)
{
	unsigned short		port = ntohs(tcp->th_sport);
	s_response			*current_response = NULL;

	for (size_t i = 0; i < MAX_PORT; i++)
	{
		if (responses[i].port == NOT_A_PORT)
		{
			break ;
		}
		if (responses[i].port == port)
		{
			current_response = responses + i;
			break ;
		}
	}
	if (current_response != NULL)
	{
		register_tcp(eth, ip, tcp, current_response);
	}
}
