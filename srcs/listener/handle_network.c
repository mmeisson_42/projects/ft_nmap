
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#include "ft_nmap.h"
#include "listeners.h"
#include "print_network.h"
#include "hash_map.h"

#include <stdio.h>
static void		handle_network_back(
	struct ethhdr *eth,
	struct iphdr *iphdr,
	void *layer4,
	struct s_hashmap *map
)
{
	char				*address = inet_ntoa(*(struct in_addr *)&iphdr->saddr);
	struct s_response	*responses;

	if (address == NULL)
	{
		return ;
	}
	responses = ft_hashmap_get(map, address, ft_strlen(address) + 1);
	if (responses == NULL)
	{
		return ;
	}
	switch (iphdr->protocol)
	{
		case IPPROTO_TCP:
			handle_tcp(eth, iphdr, layer4, responses);
			break ;
		case IPPROTO_UDP: // UDP responses are rare, and always indicate that the port is open
			handle_udp_scan_response(eth, iphdr, layer4, 1, responses);
// 			break ;
		case IPPROTO_ICMP: // Actually, it seems that an UDP scan will get an ICMP_RESPONSE most of the time
			handle_udp_scan_response(eth, iphdr, layer4, 0, responses);
			break ;
		default:
			return ; // Not handled
	}
}

int		handle_network(
	__attribute__((unused)) struct pcap_pkthdr *header,
	u_char *packet,
	void *param
)
{
	struct s_hashmap	*map = param;

	handle_network_back(
		(struct ethhdr *)packet,
		(struct iphdr *)(packet + sizeof(struct ethhdr) + 2),
		packet + sizeof(struct ethhdr) + sizeof(struct iphdr) + 2,
		map
	);
	return 0;
}
