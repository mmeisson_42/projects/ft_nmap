#ifndef LISTENERS_H
# define LISTENERS_H

# include <netinet/ether.h>
# include <netinet/ip.h>
# include <netinet/tcp.h>
# include <netinet/udp.h>
# include <pcap.h>
# include <pthread.h>

# include "ft_nmap.h"
# include "command_args.h"
# include "hash_map.h"

void		listener_entry(struct s_hashmap *map, s_options *opts);

void		handle_udp_scan_response(
	struct ethhdr	*eth,
	struct iphdr	*ip,
	void			*layer4,
	int				is_udp,
	s_response		*responses
);

void		handle_tcp(
	struct ethhdr *eth,
	struct iphdr *ip,
	struct tcphdr *tcp,
	s_response *responses
);

int			handle_network(
	struct pcap_pkthdr *header,
	u_char *packet,
	void *param
);

#endif
