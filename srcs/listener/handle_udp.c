
#include "ft_nmap.h"
#include "helpers.h"
#include "listeners.h"

static void		parse_icmp(
	struct ethhdr *eth,
	struct iphdr *ip,
	struct icmphdr *icmp,
	s_response *current_response
)
{
	(void)eth;
	(void)ip;
	if (icmp->type == 3 && icmp->code == 3)
	{
		current_response->scan_state[SCAN_UDP] = CLOSE;
	}
	else
	{
		current_response->scan_state[SCAN_UDP] = FILTERED;
	}
}

void		handle_udp_scan_response(
	struct ethhdr	*eth,
	struct iphdr	*ip,
	void			*layer4,
	int				is_udp,
	s_response		*responses
)
{
	struct udphdr	*udp_header;
	struct icmphdr	*icmp_header = layer4;
	unsigned short		port;
	s_response			*current_response = NULL;

	if (is_udp)
	{
		udp_header = layer4;
		port = ntohs(udp_header->uh_sport);
	}
	else
	{
		udp_header = (struct udphdr *)((unsigned char *)layer4 + sizeof(struct icmphdr) + sizeof(struct iphdr));
		port = ntohs(udp_header->uh_dport);
	}
	for (size_t i = 0; i < MAX_PORT; i++)
	{
		if (responses[i].port == NOT_A_PORT)
		{
			break ;
		}
		if (responses[i].port == port)
		{
			current_response = responses + i;
			break ;
		}
	}
	if (current_response != NULL)
	{
		if (is_udp == 1)
		{
			current_response->scan_state[SCAN_UDP] = OPEN;
		}
		else
		{
			parse_icmp(eth, ip, icmp_header, current_response);
		}
	}
}
