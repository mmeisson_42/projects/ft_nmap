
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <pthread.h>

#include "helpers.h"
#include "ft_nmap.h"
#include "send_raw_packet.h"

int		send_raw_packet(
	int socket_fd,
	s_raw *data,
	const char *dest_addr,
	const char *interface,
	size_t data_size
)
{
	struct sockaddr_in		sin = {
		.sin_addr.s_addr = inet_addr(dest_addr),
	};
	static pthread_mutex_t	m = PTHREAD_MUTEX_INITIALIZER;
	int						sendto_res = -1;

	pthread_mutex_lock(&m);
	if (setsockopt(socket_fd, SOL_SOCKET, SO_BINDTODEVICE, interface, ft_strlen(interface)) == -1)
	{
		perror("setsockopt: ");
	}
	else
	{
		sendto_res = sendto(socket_fd, data, data_size, 0, (struct sockaddr *)&sin, sizeof sin);
	}
	pthread_mutex_unlock(&m);
	return sendto_res;
}
