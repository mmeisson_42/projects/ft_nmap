#ifndef SCANS_H
# define SCANS_H

# include <sys/types.h>
# include <unistd.h>
# include <pthread.h>
# include <netinet/in.h>
# include <arpa/inet.h>

# include "hash_map.h"
# include "command_args.h"
# include "ft_nmap.h"

# define INIT_TCP(source_port, address, dest_port) { \
		.layer3 = { \
			.ihl = (sizeof(struct ip) >> 2), \
			.version = 4, \
			.tos = 0, \
			.tot_len = htons(sizeof(struct s_tcp)), \
			.id = getpid(), \
			.ttl = DEFAULT_TTL, \
			.protocol = IPPROTO_TCP, \
			.daddr = inet_addr(address), \
		}, \
		.layer4 = { \
			.source = htons(source_port), \
			.dest = htons(dest_port), \
			.seq = htonl(0x7f1298a6), \
			.ack_seq = 0, \
			.doff = 6, \
			.window = htons(1024), \
		}, \
		.options = {0x02, 0x04, 0x05, 0xb4}, \
	}

# define INIT_UDP(source_port, address, dest_port) { \
		.layer3 = { \
			.version = 4, \
			.ihl = sizeof(struct ip) >> 2, \
			.tos = 0, \
			.tot_len = htons(sizeof(struct s_udp)), \
			.id = htons(getpid()), \
			.ttl = DEFAULT_TTL, \
			.protocol = IPPROTO_UDP, \
			.daddr = inet_addr(address), \
		}, \
		.layer4 = { \
			.source = htons(source_port), \
			.dest = htons(dest_port), \
			.check = 0, \
			.len = htons(sizeof( ((s_udp *)0)->layer4 )), \
		}, \
	}

typedef struct	s_target
{
	s_host			*host;
	int				port; /* int to have error value */
}				s_target;

void	*launch_scans(void *arg);

int		ack_scan(int socket_fd, const char *addr, unsigned short port);
int		null_scan(int socket_fd, const char *addr, unsigned short port);
int		syn_scan(int socket_fd, const char *addr, unsigned short port);
int		udp_scan(int socket_fd, const char *addr, unsigned short port);
int		xmas_scan(int socket_fd, const char *addr, unsigned short port);
int		fin_scan(int socket_fd, const char *addr, unsigned short port);
int		con_scan(const char *addr, unsigned short port, struct s_hashmap *map);

int		send_raw_packet(
	int socket_fd,
	s_raw *data,
	const char *dest_addr,
	const char *inteface,
	size_t data_size
);


#endif
