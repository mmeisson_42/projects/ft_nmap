
#include <arpa/inet.h>
#include <ifaddrs.h>

#include "scans.h"
#include "send_raw_packet.h"
#include "helpers.h"

int		fin_scan(int socket_fd, const char *address, unsigned short dest_port)
{
	s_tcp		data = INIT_TCP(g_port_table.ports[SCAN_FIN], address, dest_port);
	s_interface	interface = get_interface(address);

	/* Set interface where we expect to get binded */
	data.layer3.saddr = interface.interface_addr.s_addr;

	/* Set scan's flag(s) */
	data.layer4.fin = 1;

	/* Compute tcp checksum */
	data.layer4.check = 0;
	data.layer4.check = tcp_checksum(
		&data.layer4,
		sizeof(data.layer4) + sizeof data.options,
		data.layer3.saddr,
		data.layer3.daddr
	);

	int res = send_raw_packet(
		socket_fd,
		(s_raw *)&data,
		address,
		interface.interface_name,
		sizeof data
	);
	free(interface.interface_name);
	return res;
}
