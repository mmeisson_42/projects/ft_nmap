
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <poll.h>

#include "hash_map.h"
#include "command_args.h"
#include "ft_nmap.h"

static void		poll_fd(int fd, s_response *response)
{
	struct pollfd	pollfd = {
		.fd = fd,
		.events = POLLOUT,
		.revents = 0
	};
	int			res;

	res = poll(&pollfd, 1, 2);
	if (res == -1)
	{
		switch (errno)
		{
			case ECONNREFUSED:
				response->scan_state[SCAN_CON] = CLOSE;
				break ;
			case ENETUNREACH:
				response->scan_state[SCAN_CON] = FILTERED;
				break ;
		}
	}
	else
	{
		response->scan_state[SCAN_CON] = OPEN;
	}
}

int		con_scan(const char *addr, unsigned short port, struct s_hashmap *map)
{
	int					socket_fd;
	int					connected;
	s_response			*responses;
	struct timeval timeout = {
		.tv_sec = 2,
		.tv_usec = 0
	};
	struct sockaddr_in	sin = {
		.sin_family = AF_INET,
		.sin_port = htons(port),
		.sin_addr = {
			.s_addr = inet_addr(addr)
		},
	};
	struct protoent		*proto = getprotobyname("tcp");

	responses = ft_hashmap_get(map, addr, ft_strlen(addr) + 1);
	if (responses != NULL)
	{
		for (size_t i = 0; i < MAX_PORT; i++)
		{
			if (responses[i].port == NOT_A_PORT)
			{
				break ;
			}
			if (responses[i].port == port)
			{
				responses = responses + i;;
				break ;
			}
		}

		socket_fd = socket(AF_INET, SOCK_STREAM, proto->p_proto);
		if (socket_fd != -1)
		{
			setsockopt(socket_fd, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof timeout);
			connected = connect(socket_fd, (struct sockaddr *)&sin, sizeof sin);
			if (connected != -1)
			{
				responses->scan_state[SCAN_CON] = OPEN;
				close(connected);
			}
			else
			{
				switch (errno)
				{
					case ECONNREFUSED:
						responses->scan_state[SCAN_CON] = CLOSE;
						break ;
					case EINPROGRESS:
						poll_fd(socket_fd, responses);
					case ETIMEDOUT:
					case ENETUNREACH:
						responses->scan_state[SCAN_CON] = FILTERED;
						break ;
				}
			}
			close(socket_fd);
		}
		else
			perror(__FILE__);
	}
	return 0;
}
