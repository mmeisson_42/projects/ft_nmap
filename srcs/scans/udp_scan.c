
#include "scans.h"
#include "send_raw_packet.h"
#include "helpers.h"

int		udp_scan(int socket_fd, const char *address, unsigned short dest_port)
{
	s_udp		data = INIT_UDP(g_port_table.ports[SCAN_UDP], address, dest_port);
	s_interface	interface = get_interface(address);

	/* Set interface where we expect to get binded */
	data.layer3.saddr = interface.interface_addr.s_addr;

	int res = send_raw_packet(
		socket_fd,
		(s_raw *)&data,
		address,
		interface.interface_name,
		sizeof data
	);
	free(interface.interface_name);
	return res;
}
