
#include "scans.h"

static s_target		get_next_port(s_options *options)
{
	static int				index = 0;
	static struct s_list	*hosts = NULL;
	static pthread_mutex_t	mutex = PTHREAD_MUTEX_INITIALIZER;
	s_target				ret = { .port = -1, .host = NULL };

	pthread_mutex_lock(&mutex);
	if (hosts == NULL)
	{ /* First time we are there */
		hosts = options->hosts;
	}
	if (hosts == (void *)1)
	{ /* Nothing to do, all scan sent */
		pthread_mutex_unlock(&mutex);
		return ret;
	}
	if (index >= MAX_PORT || options->ports[index] == NOT_A_PORT)
	{
		hosts = hosts->next;
		index = 0;
		if (hosts == NULL)
		{
			hosts = (void *)1;
			pthread_mutex_unlock(&mutex);
			return ret;
		}
	}
	ret.port = options->ports[index++];
	ret.host = hosts->content;
	pthread_mutex_unlock(&mutex);
	return ret;
}

void	*launch_scans(void *arg)
{
	void				**args = arg;
	s_target			target;
	struct s_hashmap	*map = (struct s_hashmap *)args[0];
	s_options			*options = (s_options*)args[1];

	target = get_next_port(options);
	while (target.port != -1)
	{
		if (options->mask_scans & REQ_SCAN_ACK)
		{
			ack_scan(options->socket_fd, target.host->addressv4, target.port);
			usleep(options->slow_sleep);
		}
		if (options->mask_scans & REQ_SCAN_NULL)
		{
			null_scan(options->socket_fd, target.host->addressv4, target.port);
			usleep(options->slow_sleep);
		}
		if (options->mask_scans & REQ_SCAN_FIN)
		{
			fin_scan(options->socket_fd, target.host->addressv4, target.port);
			usleep(options->slow_sleep);
		}
		if (options->mask_scans & REQ_SCAN_SYN)
		{
			syn_scan(options->socket_fd, target.host->addressv4, target.port);
			usleep(options->slow_sleep);
		}
		if (options->mask_scans & REQ_SCAN_UDP)
		{
			udp_scan(options->socket_fd, target.host->addressv4, target.port);
			usleep(options->slow_sleep);
		}
		if (options->mask_scans & REQ_SCAN_XMAS)
		{
			xmas_scan(options->socket_fd, target.host->addressv4, target.port);
			usleep(options->slow_sleep);
		}
		if (options->mask_scans & REQ_SCAN_CON)
		{
			con_scan(target.host->addressv4, target.port, map);
			usleep(options->slow_sleep);
		}
		target = get_next_port(options);
	}
	alarm(1);
	return NULL;
}
