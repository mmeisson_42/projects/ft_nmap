
#include "libft.h"
#include "command_args.h"
#include "helpers.h"

void	print_configuration(s_options *opts)
{
	size_t		ports_number = 0;

	for (size_t i = 0; i < MAX_PORT && opts->ports[i] != NOT_A_PORT; i++)
	{
		ports_number++;
	}
	puts(">>> Scan configuration <<<");
	printf("\tTarget ip addresses:");
	for (
		struct s_list *host = opts->hosts;
		host != NULL;
		host = host->next
	)
	{
		printf(" %s", ((struct s_host *)host->content)->addressv4);
	}
	puts("");
	printf("\tNb of ports to scan: %zu\n", ports_number);

	printf("\tScans to be performed:"); /* Could be even more compact :D */
	if (opts->mask_scans & REQ_SCAN_NULL) { printf(" "SCAN_NULL_STR); }
	if (opts->mask_scans & REQ_SCAN_ACK) { printf(" "SCAN_ACK_STR); }
	if (opts->mask_scans & REQ_SCAN_FIN) { printf(" "SCAN_FIN_STR); }
	if (opts->mask_scans & REQ_SCAN_SYN) { printf(" "SCAN_SYN_STR); }
	if (opts->mask_scans & REQ_SCAN_UDP) { printf(" "SCAN_UDP_STR); }
	if (opts->mask_scans & REQ_SCAN_XMAS) { printf(" "SCAN_XMAS_STR); }
	puts("");

	printf("\tNb of threads: %u\n\n", opts->threads_number);
}
