
#include "ft_nmap.h"

static const	char	*scans_map[] = {
	[ SCAN_NULL ] = SCAN_NULL_STR,
	[ SCAN_ACK ] = SCAN_ACK_STR,
	[ SCAN_FIN ] = SCAN_FIN_STR,
	[ SCAN_SYN ] = SCAN_SYN_STR,
	[ SCAN_UDP ] = SCAN_UDP_STR,
	[ SCAN_XMAS ] = SCAN_XMAS_STR,
	[ SCAN_CON ] = SCAN_CON_STR,
};

const char	*scan_idx_to_str(unsigned int scan)
{
	if (scan < ARRAY_LEN(scans_map))
	{
		return scans_map[scan];
	}
	return NULL;
}
