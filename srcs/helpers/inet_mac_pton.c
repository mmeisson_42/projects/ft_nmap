
#include "libft.h"
#include "helpers.h"

int		inet_mac_pton(char *mac_address, unsigned char *bytes)
{
	char	*values[6] = { 0 };
	size_t	mac_index = 0;
	size_t	i;

	ft_memset(bytes, 0, 6);
	for (i = 0; i < 6; i++)
	{
		values[i] = &mac_address[mac_index];

		// Check for first char minima
		if (ft_strchr(HEX_CHARS, mac_address[mac_index]) == NULL)
			return -1;
		mac_index++;

		// Does we have a second one
		if (ft_strchr(HEX_CHARS, mac_address[mac_index]) != NULL)
			mac_index++;
		if (mac_address[mac_index] != SEPARATOR || i == 5)
			return -1;
		mac_address[mac_index] = 0;
		bytes[i] = (unsigned short)ft_atoi_hexa(values[i]);
	}
	return 0;
}
