
#include "ft_nmap.h"

static const char	*state_map[] = {
	[ OPEN ] = OPEN_STR,
	[ UNFILTERED ] = UNFILTERED_STR,
	[ FILTERED ] = FILTERED_STR,
	[ OPEN_FILTERED ] = OPEN_FILTERED_STR,
	[ CLOSE ] = CLOSE_STR,
};

const char	*state_to_str(e_service_state state)
{
	if (state >= 0 && (unsigned int)state < ARRAY_LEN(state_map))
	{
		return state_map[state];
	}
	return NULL;
}
