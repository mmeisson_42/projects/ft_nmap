
#include "libft.h"
#include "helpers.h"

char		*resolve_host(const char *host_name)
{
	char			*addr = NULL;
	struct hostent	*h;
	struct in_addr	**ai;

	h = gethostbyname(host_name);
	if (h != NULL)
	{
		ai= (struct in_addr **)h->h_addr_list;
		for (size_t i = 0; ai[i] != NULL; i++)
		{
			char *resolved = inet_ntoa(*(ai[0]));

			if (resolved != NULL)
			{
				addr = ft_strdup(resolved);
				break ;
			}
		}
	}
	return addr;
}
