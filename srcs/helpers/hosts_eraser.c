
#include <stdlib.h>

#include "command_args.h"

void	hosts_eraser(void * data, __attribute__((unused))size_t data_size)
{
	struct s_host	*host = data;

	free(host->name);
	free(host->addressv4);
	free(host);
}

