
#include "ft_nmap.h"
#include "libft.h"

const char		*available_scans[] = {
	[SCAN_NULL] = SCAN_NULL_STR,
	[SCAN_ACK] = SCAN_ACK_STR,
	[SCAN_FIN] = SCAN_FIN_STR,
	[SCAN_SYN] = SCAN_SYN_STR,
	[SCAN_UDP] = SCAN_UDP_STR,
	[SCAN_XMAS] = SCAN_XMAS_STR,
	[SCAN_CON] = SCAN_CON_STR,
};

const	int		scan_flags[] = {
	REQ_SCAN_NULL,
	REQ_SCAN_ACK,
	REQ_SCAN_FIN,
	REQ_SCAN_SYN,
	REQ_SCAN_UDP,
	REQ_SCAN_XMAS,
	REQ_SCAN_CON
};

int		get_scan_code(const char *str)
{
	FOR_ARRAY (available_scans, type)
	{
		if (available_scans[type] && ft_strcmp(str, available_scans[type]) == 0)
		{
			return scan_flags[type];
		}
	}
	return -1;
}
