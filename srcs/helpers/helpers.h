#ifndef HELPERS_H
# define HELPERS_H

# include <arpa/inet.h>
# include <ifaddrs.h>
# include <netdb.h>
# include <netinet/in.h>
# include <pcap.h>
# include <signal.h>
# include <stdlib.h>
# include <stdio.h>
# include <stdint.h>
# include <sys/time.h>
# include <sys/types.h>

# include "command_args.h"
# include "hash_map.h"

# define USEC_PER_SEC	1000000
# define USEC_PER_MSEC	1000
# define LOCAL "lo"
# define IS_LOCAL(interface) (ft_strcmp(interface, LOCAL) == 0)
# define FORMAT_INTERFACE "/sys/class/net/%s/address"
# define MAC_ADDRESS_LEN 18
# define HEX_CHARS	"0123456789abcdef"
# define SEPARATOR	':'

typedef struct		s_interface
{
	char			*interface_name;
	struct in_addr	interface_addr;
}					s_interface;

uint16_t		tcp_checksum(const void *buff, size_t len, in_addr_t src_addr,
						in_addr_t dest_addr);
double			compute_latency(struct timeval *origin);
char			*get_host_by_addr(const char *addr4);
struct ifaddrs	*get_interface_address(int local);
char			*get_mac_addr(const char *interface_name);
int				get_scan_code(const char *str);
char			*get_service_name_by_port(unsigned short port, char *proto);
int				inet_mac_pton(char *mac_address, unsigned char *bytes);
void			print_configuration(s_options *opts);
void			print_resume(s_options *opts, struct s_hashmap *map);
char			*resolve_host(const char *host_name);
int				scan_idx_to_mask(unsigned int idx);
const char		*scan_idx_to_str(unsigned int scan);
const char		*state_to_str(e_service_state state);
char			*craft_filter(s_options *ops);
s_interface		get_interface(const char *address);
void			hosts_eraser(void *content, size_t size);
char			*get_host_by_addr(const char *addr);

#endif
