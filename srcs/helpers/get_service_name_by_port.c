
#include "libft.h"
#include "ft_nmap.h"
#include "helpers.h"

char	*get_service_name_by_port(unsigned short port, char *proto)
{
	struct servent		*s = getservbyport(htons(port), proto);

	if (s != NULL && s->s_name != NULL)
	{
		return ft_strdup(s->s_name);
	}
	return ft_strdup(UNASSIGNED);
}
