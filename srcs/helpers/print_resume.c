
#include "libft.h"
#include "ft_nmap.h"
#include "command_args.h"
#include "helpers.h"
#include "scans.h"

/*
**	This file shows a good sample of not how to use printf
*/

static void		print_port(unsigned short port)
{
	printf("\r%8hu\n", port);
}

static void		print_services(unsigned short port)
{
	char *service_tcp_name = get_service_name_by_port(port, "tcp");
	char *service_udp_name = get_service_name_by_port(port, "udp");

	if (service_tcp_name == NULL || service_udp_name == NULL)
	{
		perror(__FILE__);
		exit(1);
	}
	printf(
		"%12s|%4s%s(TCP), %s(UDP)      |",
		"",
		"",
		service_tcp_name,
		service_udp_name
	);
	free(service_tcp_name);
	free(service_udp_name);
}

static int		print_results(
	s_options *opts,
	s_response *responses
)
{
	int		opened = 0;

	FOR_ARRAY (responses->scan_state, scan)
	{
		int		mask = scan_idx_to_mask(scan);
		if (mask != -1 && (opts->mask_scans & mask))
		{
			if (responses->scan_state[scan] == OPEN)
			{
				opened = 1;
			}
			printf(
					"%12s|%42s|%4s%s(%s)\n",
					"", "", "",
					scan_idx_to_str(scan),
					state_to_str(responses->scan_state[scan])
				  );
		}
	}
	return opened;
}

static int		is_open(s_options *opts, s_response *responses)
{
	FOR_ARRAY (responses->scan_state, scan)
	{
		int		mask = scan_idx_to_mask(scan);
		if (mask != -1 && (opts->mask_scans & mask))
		{
			if (responses->scan_state[scan] == OPEN)
			{
				return 1;
			}
		}
	}
	return 0;
}

static void		print_verbose(s_options *opts, s_response *responses, s_host *host, unsigned int port_index)
{
	int			opened;
	char		*reversed;

	reversed = get_host_by_addr(host->addressv4);
	printf("\nHost %s (%s):\n", reversed, host->addressv4);
	ft_strdel(&reversed);
	printf("    Port    |     Services                             |         Reports\n");
	printf("------------+------------------------------------------+----------------------------\n");

	opened = print_results(opts, &responses[port_index]);
	print_services(opts->ports[port_index]);
	print_port(opts->ports[port_index]);
	printf("------------+------------------------------------------+----------------------------\n");

	printf("Conclusion: %s\n\n", opened ? OPEN_STR : CLOSE_STR);
}

void			print_resume(s_options *opts, struct s_hashmap *map)
{
	char	*reversed;

	for (
		struct s_list	*host_node = opts->hosts;
		host_node != NULL;
		host_node = host_node->next
	)
	{
		s_host	*host = host_node->content;
		s_response		*responses = ft_hashmap_get(map, host->addressv4, ft_strlen(host->addressv4) + 1);

		if ((opts->mask_options & CODE_NO_VERBOSE) != 0)
		{
			reversed = get_host_by_addr(host->addressv4);
			printf("\nHost %s (%s):\n", reversed, host->addressv4);
			ft_strdel(&reversed);
		}
		puts("Ports opened:");
		for (
			unsigned int port_index = 0;
			port_index < MAX_PORT && opts->ports[port_index] != NOT_A_PORT;
			port_index++
		)
		{

			int opened = is_open(opts, &responses[port_index]);

			if (opened == 1)
			{
				if ((opts->mask_options & CODE_NO_VERBOSE) == 0)
					print_verbose(opts, responses, host, port_index);
				else
					printf("%hu,", opts->ports[port_index]);
			}
		}
		if ((opts->mask_options & CODE_NO_VERBOSE) != 0)
			putchar('\n');

		puts("Ports closed:");
		for (
			unsigned int port_index = 0;
			port_index < MAX_PORT && opts->ports[port_index] != NOT_A_PORT;
			port_index++
		)
		{

			int opened = is_open(opts, &responses[port_index]);

			if (opened == 0)
			{
				if ((opts->mask_options & CODE_NO_VERBOSE) == 0)
					print_verbose(opts, responses, host, port_index);
				else
					printf("%hu,", opts->ports[port_index]);
			}
		}
		if ((opts->mask_options & CODE_NO_VERBOSE) != 0)
			putchar('\n');
	}
}
