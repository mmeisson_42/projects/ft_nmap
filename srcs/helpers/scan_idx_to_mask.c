
#include "ft_nmap.h"

static const int		mask_map[] = {
	[ SCAN_NULL ] = REQ_SCAN_NULL,
	[ SCAN_SYN ] = REQ_SCAN_SYN,
	[ SCAN_ACK ] = REQ_SCAN_ACK,
	[ SCAN_FIN ] = REQ_SCAN_FIN,
	[ SCAN_UDP ] = REQ_SCAN_UDP,
	[ SCAN_XMAS ] = REQ_SCAN_XMAS,
	[ SCAN_CON ] = REQ_SCAN_CON,
};

int		scan_idx_to_mask(unsigned int idx)
{
	if (idx < ARRAY_LEN(mask_map))
	{
		return mask_map[idx];
	}

	return -1;
}
