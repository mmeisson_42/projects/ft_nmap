
#include "libft.h"
#include "helpers.h"

struct ifaddrs	*realloc_interface(struct ifaddrs *origin)
{
	struct ifaddrs	*new;

	new = malloc(sizeof(*origin) + ft_strlen(origin->ifa_name) + 1 + sizeof(struct sockaddr_in));
	if (new != NULL)
	{
		ft_bzero(new, sizeof(*new));
		new->ifa_name = (void*)new + sizeof(*origin);
		new->ifa_addr = (void*)new + sizeof(*origin) + ft_strlen(origin->ifa_name) + 1;
		ft_strcpy(new->ifa_name, origin->ifa_name);
		ft_memcpy(new->ifa_addr, origin->ifa_addr, sizeof(struct sockaddr_in));
	}
	return new;
}

struct ifaddrs	*get_interface_address(int local)
{
	struct ifaddrs		*lst = NULL;
	struct ifaddrs		*iter;
	struct ifaddrs		*interface = NULL;

	if (getifaddrs(&lst) >= 0)
	{
		iter = lst;
		while (iter != NULL)
		{
			if (iter->ifa_addr->sa_family == AF_INET
				&& IS_LOCAL(iter->ifa_name) == !!local)
			{
				interface = realloc_interface(iter);
				break ;
			}
			iter = iter->ifa_next;
		}
		freeifaddrs(lst);
	}
	return interface;
}
