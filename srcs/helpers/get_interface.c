
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>

#include "libft.h"
#include "helpers.h"

s_interface		get_interface(const char * address)
{
	struct ifaddrs		*interface;
	s_interface			res = { 0 };
	int					local = 0;

	if (ft_strcmp(address, "0.0.0.0") == 0 || ft_strcmp(address, "127.0.0.1") == 0)
	{
		local = 1;
	}
	interface = get_interface_address(local);
	if (interface != NULL)
	{
		res.interface_name = ft_strdup(interface->ifa_name);
		res.interface_addr = ((struct sockaddr_in *)interface->ifa_addr)->sin_addr;
	}
	else
	{
		res.interface_name = ft_strdup("");
	}
	if (res.interface_name == NULL)
	{
		perror(__FILE__);
		exit(EXIT_FAILURE);
	}
	if (interface != NULL)
		free(interface);
	return res;
}
