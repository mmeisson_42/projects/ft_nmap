
#include "libft.h"
#include "ft_nmap.h"
#include "command_args.h"
#include "helpers.h"

extern	s_port_table		g_port_table;

static char		**get_ports(s_options *opts)
{
	char	**ports_str = NULL;

	if (ft_tabappend(&ports_str, "icmp or") == NULL)
	{
		return NULL;
	}
	for (size_t type = 0; type < SCAN_TYPES; type++)
	{
		if (opts->mask_scans & scan_idx_to_mask(type))
		{
			char	*port_str = ft_itoa(g_port_table.ports[type]);

			if (port_str == NULL)
			{
				return NULL;
			}
			if (ft_tabappend(&ports_str, "and dst port ") == NULL)
			{
				return NULL;
			}
			if (ft_tabappend(&ports_str, port_str) == NULL)
			{
				return NULL;
			}
			free(port_str);
		}
	}
	return ports_str;
}

char			*craft_filter(s_options *opts)
{
	char	**ports_str;
	char	*filter;

	ports_str = get_ports(opts);
	if (ports_str == NULL)
	{
		perror(__FILE__);
		return NULL;
	}
	filter = ft_tabjoin((const char **)ports_str, ' ');
	if (ports_str == NULL)
	{
		perror(__FILE__);
		return NULL;
	}
	ft_tabdel(&ports_str);
	return filter;
}
