
#include "libft.h"
#include "helpers.h"

char	*get_mac_address(const char *interface_name)
{
	FILE		*interface;
	char		address[MAC_ADDRESS_LEN + 1] = { 0 };
	char		file_name[255 + sizeof FORMAT_INTERFACE];
	char		*str_address = NULL;

	if (sprintf(file_name, FORMAT_INTERFACE, interface_name) < 0)
	{
		return NULL;
	}
	interface = fopen(file_name, "r");
	if (interface != NULL)
	{
		fread(address, MAC_ADDRESS_LEN, 1, interface);
		str_address = ft_strdup(address);
		fclose(interface);
	}
	return str_address;
}
