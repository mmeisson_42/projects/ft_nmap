
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include "print_network.h"
#include "ft_nmap.h"

static void		print_ethernet(struct ethhdr *eth)
{
	printf(
		"Ethernet {\n"
		"	.dest = "
	);
	PRINT_MAC(eth->h_dest);

	printf("\n\t.source = ");
	PRINT_MAC(eth->h_source);
	

	printf(
		"\n\t.proto = %hu\n"
		"}\n",
		eth->h_proto
	);
}

static void		print_ip(struct iphdr *iphdr)
{
	printf(
		"Ip {\n"
		"	.ihl + .version = %x\n"
		"	.tos = %hhu\n"
		"	.tot_len = %hu\n"
		"	.id = %hu\n"
		"	.frag_off = %hu\n"
		"	.ttl = %hhu\n"
		"	.protocol = %hhu\n"
		"	.check = %#.4hx\n"
		"	.saddr = %s\n"
		"	.daddr = %s\n"
		"}\n",
		*(unsigned int *)iphdr,
		iphdr->tos,
		ntohs(iphdr->tot_len),
		ntohs(iphdr->id),
		ntohs(iphdr->frag_off),
		iphdr->ttl,
		iphdr->protocol,
		ntohs(iphdr->check),
		inet_ntoa(*(struct in_addr *)&iphdr->saddr),
		inet_ntoa(*(struct in_addr *)&iphdr->daddr)
	);
}

static void		print_tcp(struct tcphdr *tcp)
{
	printf(
		"Tcp {\n"
		"	.th_sport = %hu\n"
		"	.th_dport = %hu\n"
		"	.th_seq = %u\n"
		"	.th_ack = %u\n"
		"\n"
		"	.th_flags %#.2hhx\n"
		"	.th_win %hu\n"
		"	.th_sum %hu\n"
		"	.th_urp %hu\n"
		"}\n",
		ntohs(tcp->th_sport),
		ntohs(tcp->th_dport),
		ntohl(tcp->th_seq),
		ntohl(tcp->ack_seq),
		tcp->th_flags,
		ntohs(tcp->th_win),
		ntohs(tcp->th_sum),
		ntohs(tcp->th_urp)
	);
}

void			print_network(u_char *network)
{
	struct ethhdr	*eth = (struct ethhdr *)network;
	struct iphdr	*iphdr = (struct iphdr *)(network + sizeof(struct ethhdr) + 2);
	void			*layer4 = (void *)(network  + 2 + sizeof(struct ethhdr) + sizeof(struct iphdr));

	puts(
		"##########################################\n"
		"##########################################"
	);

	print_ethernet(eth);
	print_ip(iphdr);

	switch (iphdr->protocol)
	{
		case IPPROTO_TCP:
			print_tcp((struct tcphdr *)layer4);
			break ;
		default:
			print_tcp((struct tcphdr *)layer4);
			printf("Protocol number %hhu is not displayable atm\n", iphdr->protocol);
	}

#include "hexdump.h"
	hexdump(eth, sizeof *eth);
	hexdump(iphdr, sizeof *iphdr);
	hexdump(layer4, sizeof(struct tcphdr));

	puts(
		"##########################################\n"
		"##########################################\n\n\n"
	);
}
