
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pcap/pcap.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>

#include "ft_nmap.h"
#include "command_args.h"
#include "scans.h"
#include "libft.h"
#include "helpers.h"
#include "print_network.h"
#include "init_responses.h"
#include "listeners.h"


s_port_table	g_port_table;

static void	response_map_erase(void * data, __attribute__((unused))size_t data_size)
{
	struct s_node	*node = data;

	free(node->content);
}

static void	set_port_table(void)
{
	uint16_t			port = USHRT_MAX;
	struct sockaddr_in	server_addr;

	ft_bzero(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);
	server_addr.sin_addr.s_addr = INADDR_ANY;
	for (size_t i = 0; i < SCAN_TYPES; i++)
	{
		g_port_table.sockets[i] = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (g_port_table.sockets[i] == -1)
		{
			dprintf(2, "Fatal: Couldn't create socket\n");
			exit(1);
		}
		while (bind(g_port_table.sockets[i], (struct sockaddr*)&server_addr,
				sizeof(server_addr)) == -1)
		{
			server_addr.sin_port = htons(port--);
			if (port == 0)
			{
				dprintf(2, "Fatal: Couldn't find available ports\n");
				exit(1);
			}
		}
		g_port_table.ports[i] = port;
	}
}

int			main(int ac, char **av)
{
	s_options			opts = { 0 };
	s_response			responses[MAX_PORT];
	struct s_hashmap	map;
	struct timeval		tv;

	/* Retrieve options from command line */
	arg_manage(ac, av, &opts);

	/* Reserve some ports to send packets */
	set_port_table();

	if ((opts.mask_scans & REQ_SCAN_ROOT))
	{
		opts.socket_fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
		if (opts.socket_fd == -1)
		{
			perror("Socket: ");
			return 1;
		}
	}
	else
	{
		opts.socket_fd = -1;
	}

	print_configuration(&opts);

	/* Set responses to their default values */
	init_responses(&opts, responses);
	ft_hashmap_init(&map);
	for (
		struct s_list *iterator = opts.hosts;
		iterator != NULL;
		iterator = iterator->next
	)
	{
		struct s_host	*h = iterator->content;

		ft_hashmap_add(
			&map,
			h->addressv4,
			ft_strlen(h->addressv4) + 1,
			&responses,
			sizeof responses
		);
	}

	/* Let's work */
	gettimeofday(&tv, NULL);
	listener_entry(&map, &opts);
	puts("Scanning... done");
	printf("Scan took arround %.2Fms\n\n", compute_latency(&tv));
	print_resume(&opts, &map);
	ft_hashmap_erase(&map, response_map_erase);
	ft_lstdel(&opts.hosts, hosts_eraser);
	return EXIT_SUCCESS;
}
