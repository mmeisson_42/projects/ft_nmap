
#ifndef COMMAND_ARGS_H
# define COMMAND_ARGS_H

# include <fcntl.h>
# include <limits.h>
# include <netinet/in.h>
# include <stdio.h>
# include <stdlib.h>

# include "libft.h"
# include "ft_nmap.h"

# define OPT_HELP		"--help"
# define OPT_PORTS		"--ports"
# define OPT_IP			"--ip"
# define OPT_THREADS	"--speedup"
# define OPT_SCAN_TYPE	"--scan"
# define OPT_FILE		"--file"
# define OPT_NEURONE	"--neurone"
# define OPT_NO_VERBOSE	"--no-verbose"
# define OPT_SLOW_MODE	"--slow"

# define CODE_HELP		(1 << 0)
# define CODE_PORTS		(1 << 1)
# define CODE_IP		(1 << 2)
# define CODE_THREADS	(1 << 3)
# define CODE_SCAN_TYPE	(1 << 4)
# define CODE_FILE		(1 << 5)
# define CODE_NEURONE	(1 << 6)
# define CODE_NO_VERBOSE	(1 << 7)
# define CODE_SLOW_MODE	(1 << 8)

# define MAX_PORT		1024
# define NOT_A_PORT		(USHRT_MAX + 1)

# define SLOW_NINJA		"NINJA"
# define SLOW_TIRED		"TIRED"
# define SLOW_AWAKEN	"AWAKEN"
# define SLOW_COFFEE	"COFFEE"
# define SLOW_COCAINE	"COCAINE"

/*
**	usleep timers
*/
# define NINJA_SLEEP	2000000
# define TIRED_SLEEP	1000000
# define AWAKEN_SLEEP	500000
# define COFFEE_SLEEP	100000
# define COCAINE_SLEEP	0

/*
**	Data types
*/

typedef struct			s_host
{
	char			*name;
	char			*addressv4;
}						s_host;

typedef struct			s_options
{
	unsigned int	mask_options;
	unsigned int	ports[MAX_PORT]; /* int to have a value for NONE */
	struct s_list	*hosts;
	unsigned int	threads_number;
	unsigned int	mask_scans;
	char			file_name[NAME_MAX];
	int				socket_fd;
	size_t			slow_sleep;
}						s_options;

typedef struct			s_dataoption
{
	char		const * const text;
	int			(*callback)(int *, char **, s_options *);
}						s_dataoption;

int		arg_parse(int ac, char **av, s_options *opts);
int		arg_manage(int ac, char **av, s_options *opts);

int		call_helper(int *index, char **argv, s_options *opts);
int		set_ports(int *index, char **argv, s_options *opts);
int		set_ips(int *index, char **argv, s_options *opts);
int		handle_file(int *index, char **argv, s_options *opts);
int		set_threads(int *index, char **argv, s_options *opts);
int		set_scans(int *index, char **argv, s_options *opts);
int		get_neurone(int *index, char **argv, s_options *opts);
int		set_no_verbose(int *index, char **argv, s_options *opts);
int		set_slow_mode(int *index, char **argv, s_options *opts);

void	print_helper(const char *prog_name, int verbose);
int		register_host(const char *name, struct s_list **hosts);


#endif
