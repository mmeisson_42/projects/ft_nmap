
#include "ft_nmap.h"
#include "command_args.h"
#include "helpers.h"

int		cmp_hosts(void *a, void *b)
{
	s_host	*_a = a;
	s_host	*_b = b;

	return (ft_strcmp(_a->addressv4, _b->addressv4));
}

int		arg_manage(int ac, char **av, s_options *opts)
{
	int		result_parse = arg_parse(ac, av, opts);

	if (result_parse == -1)
	{
		print_helper(av[0], 0);
		exit(EXIT_FAILURE);
	}
	if (opts->mask_options & CODE_HELP)
	{
		exit(EXIT_SUCCESS);
	}
	if ((opts->mask_options & (CODE_FILE | CODE_IP)) == 0) /* None of these options */
	{
		dprintf(2, "You need to give at least one of these options : " OPT_IP " or " OPT_FILE "\n");
		print_helper(av[0], 0);
		exit(EXIT_FAILURE);
	}
	if ((opts->mask_options & CODE_THREADS) && (opts->mask_options & CODE_SLOW_MODE))
	{
		dprintf(2, "Option " OPT_THREADS " and option " OPT_SLOW_MODE " can't be used together\n");
		exit(EXIT_FAILURE);
	}
	if ((opts->mask_options & CODE_THREADS) == 0)
	{
		opts->threads_number = DEFAULT_THREADS;
	}
	if ((opts->mask_options & CODE_PORTS) == 0)
	{
		for (size_t i = 0; i < 1023; i++)
		{
			opts->ports[i] = i + 1;
		}
		opts->ports[1023] = NOT_A_PORT;
	}
	if ((opts->mask_options & CODE_SCAN_TYPE) == 0)
	{
		opts->mask_scans = REQ_SCAN_ALL;
	}

	/* If not root, return */
	if (getuid() > 0 && (opts->mask_scans & REQ_SCAN_ROOT))
	{
		dprintf(2, "Fatal: Required scans require to be launched as root\n");
		exit(EXIT_FAILURE);
	}

	opts->hosts = merge_list(opts->hosts, cmp_hosts);
	for (struct s_list *iter = opts->hosts; iter != NULL; iter = iter->next)
	{
		while (iter->next && cmp_hosts(iter->content, iter->next->content) == 0)
		{
			struct s_list *tmp = iter->next;
			iter->next = iter->next->next;

			hosts_eraser(tmp->content, tmp->content_size);
			free(tmp);
		}
	}
	return result_parse;
}
