
#include "ft_nmap.h"
#include "command_args.h"
#include "helpers.h"
#include "get_next_line.h"

int		call_helper(
	__attribute__((unused))int *index,
	char **av,
	s_options *opts
)
{
	opts->mask_options |= CODE_HELP;
	print_helper(av[0], 1);
	return 0;
}

int		set_ports(
	int *index,
	char **av,
	s_options *opts
)
{
	opts->mask_options |= CODE_PORTS;
	(*index) += 1; // i actually points to option's name, move forward
	size_t			port_index = 0;
	const char		*arg_ports = av[*index];

	if (arg_ports == NULL)
		return -1;
	char			**comma_separated = ft_strsplit(arg_ports, ',');
	if (comma_separated == NULL)
	{
		dprintf(2, "ENOMEM");
		return -1;
	}
	if (comma_separated[0] == NULL)
	{
		return -1;
	}
	for (size_t i = 0; comma_separated[i] != NULL; i++)
	{
		char	**tokens = ft_strsplit(comma_separated[i], '-');
		if (tokens == NULL)
		{
			dprintf(2, "ENOMEM");
			return -1;
		}
		for (size_t i = 0; tokens[i] != NULL; i++) // We want only numbers
			for (size_t j = 0; tokens[i][j] != 0; j++)
				if (ft_isdigit(tokens[i][j]) == 0)
				{
					ft_tabdel(&tokens);
					return -1;
				}

		switch (ft_tablen((const char **)tokens))
		{
			case 1: // Only one token : We have a raw port
				opts->ports[port_index++] = ft_atoi(tokens[0]);
				if (port_index >= MAX_PORT)
				{
					ft_tabdel(&tokens);
					return -1;
				}
				break ;
			case 2: // Two token : A range of port
				for (int i = ft_atoi(tokens[0]); i < ft_atoi(tokens[1]); i++)
				{
					opts->ports[port_index++] = i;
					if (port_index >= MAX_PORT)
					{
						ft_tabdel(&tokens);
						return -1;
					}
				}
				break ;
			default:
				ft_tabdel(&tokens);
				return -1;
		}
		ft_tabdel(&tokens);

	}
	ft_tabdel(&comma_separated);
	if (port_index < MAX_PORT)
		opts->ports[port_index] = NOT_A_PORT;
	return 0;
}

int		set_ips(
	int *index,
	char **av,
	s_options *opts
)
{
	opts->mask_options |= CODE_IP;
	(*index) += 1;
	if (av[*index] == NULL || av[*index][0] == '-')
	{
		dprintf(2, "Error: --ip takes at least one host\n");
		return -1;	// We do not have at least one ipv4 address registered
	}
	while (av[*index] != NULL && av[*index][0] != '-')
	{
		if (register_host(av[*index], &opts->hosts) == -1)
		{
			dprintf(2, "Error: %s is not a valid host name\n", av[*index]);
			return -1;
		}
		(*index)++;
	}
	(*index)--;
	return 0;
}

int		handle_file(
	int *index,
	char **av,
	s_options *opts
)
{
	opts->mask_options |= CODE_FILE;
	(*index) += 1;

	const char	*file_name = av[*index];
	int			fd;

	if (file_name == NULL)
	{
		dprintf(2, "Error: " OPT_FILE " requires a file_name\n");
		return -1;
	}
	fd = open(file_name, O_RDONLY);
	if (fd == -1)
	{
		perror(file_name);
		return -1;
	}
	int		count = 0;
	for (
		char *str = NULL;
		get_next_line(fd, &str) > 0;
		ft_strdel(&str)
	)
	{
		char	**hosts_names = ft_whitesplit(str);

		if (hosts_names == NULL)
		{
			perror("");
			close(fd);
			return -1;
		}
		for (size_t i = 0; hosts_names[i]; i++)
		{
			if (register_host(hosts_names[i], &opts->hosts) == -1)
			{
				dprintf(2, "Could not resolve host %s\n", hosts_names[i]);
				close(fd);
				ft_tabdel(&hosts_names);
				return -1;
			}
			count++;
		}
		ft_tabdel(&hosts_names);
	}
	if (count == 0)
	{
		dprintf(2, OPT_FILE" :%s does not contain any host\n", file_name);
		return -1;
	}
	close(fd);
	return 0;
}

int		set_threads(
	int *index,
	char **av,
	s_options *opts
)
{
	opts->mask_options |= CODE_THREADS;
	(*index) += 1;

	const char	*str_val = av[*index];
	int			threads;

	if (str_val == NULL)
	{
		dprintf(2, "Error: " OPT_THREADS " takes at least one numeric value\n");
		return -1;
	}

	// Is this arg a number ?
	for (size_t i = 0; str_val[i]; i++) { if (ft_isdigit(str_val[i]) == 0)
	{
		dprintf(2, "Error: " OPT_THREADS " takes at least one numeric value\n");
		return -1;
	} }

	threads = ft_atoi(str_val);
	if (threads < 0)
	{
		dprintf(2, OPT_THREADS": Cannot be lower than 0\n");
		return -1;
	}
	if (threads > 255)
	{
		dprintf(2, OPT_THREADS": Cannot be greater than 255\n");
		return -1;
	}
	opts->threads_number = threads;
	return 0;
}

int		set_scans(
	int *index,
	char **av,
	s_options *opts
)
{
	opts->mask_options |= CODE_SCAN_TYPE;
	(*index) += 1;

	const char	*required_scans = av[*index];

	if (required_scans == NULL)
	{
		return -1;
	}
	char		**array_scans = ft_strsplit(required_scans, '/');
	if (array_scans == NULL)
	{
		perror(OPT_SCAN_TYPE": ");
		return -1;
	}
	if (array_scans[0] == NULL)
	{
		dprintf(2, OPT_SCAN_TYPE": require at least one scan type\n");
		ft_tabdel(&array_scans);
		return -1;
	}
	for (size_t i = 0; array_scans[i] != NULL; i++)
	{
		int code = get_scan_code(array_scans[i]);
		if (code == SCAN_ERR)
		{
			dprintf(2, OPT_SCAN_TYPE": scan %s is not available\n", array_scans[i]);
			ft_tabdel(&array_scans);
			return -1;
		}
		else
		{
			opts->mask_scans |= code;
		}
	}
	ft_tabdel(&array_scans);
	return 0;
}

int		get_neurone(
	__attribute__((unused))int *index,
	__attribute__((unused))char **av,
	__attribute__((unused))s_options *opts
)
{
	dprintf(2, "/!\\ Fatal Error :: neurones not found /!\\\n");
	exit(1);
	return 1;
}

int		set_no_verbose(
	__attribute__((unused))int *index,
	__attribute__((unused))char **av,
	s_options *opts
)
{
	opts->mask_options |= CODE_NO_VERBOSE;
	return 0;
}

int		set_slow_mode(
	int *index,
	char **av,
	s_options *opts
)
{
	(*index)++;
	opts->mask_options |= CODE_SLOW_MODE;
	char	*mode = av[*index];
	struct {const char *text; size_t timer;}	modes[] = {
		{ .text = SLOW_NINJA, .timer = NINJA_SLEEP },
		{ .text = SLOW_TIRED, .timer = TIRED_SLEEP },
		{ .text = SLOW_AWAKEN, .timer = AWAKEN_SLEEP },
		{ .text = SLOW_COFFEE, .timer = COFFEE_SLEEP },
		{ .text = SLOW_COCAINE, .timer = COCAINE_SLEEP },
	};

	if (mode == NULL)
	{
		dprintf(2, OPT_SLOW_MODE " takes an argument\n");
		return -1;
	}
	FOR_ARRAY (modes, i)
	{
		if (ft_strcmp(modes[i].text, mode) == 0)
		{
			opts->slow_sleep = modes[i].timer;
			return 0;
		}
	}
	dprintf(2, "Error : %s is not a valid slow mode\n", mode);
	return -1;
}
