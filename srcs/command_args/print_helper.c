
#include "command_args.h"

void	print_helper(const char *prog_name, int verbose)
{
	printf(
		"Usage: %s [ "OPT_HELP" ] [ "OPT_PORTS" [NUMBER/RANGE] ] [ "OPT_IP" IPV4_ADDRESS ] [ "OPT_THREADS" THREAD_NUMBER ] [ "OPT_SCAN_TYPE" TYPE ] [ "OPT_FILE" FILE_NAME ] [ "OPT_SLOW_MODE" MODE ] [ "OPT_NEURONE" ] [ "OPT_NO_VERBOSE" ]\n",
		prog_name
	);
	if (verbose != 0)
	{
		puts(
			"\t"OPT_HELP		"\t\tPrint this help scren\n"
			"\t"OPT_PORTS		"\t\tPorts to scan (eg: 1-10 or 1,2,3 or 1,6-15)\n"
			"\t"OPT_IP			"\t\tIp addresses ( format v4 or hostname ) to scan\n"
			"\t"OPT_FILE		"\t\tFile name containing ip_addresses or host_name to scan ( blank char separated )\n"
			"\t"OPT_THREADS		"\tNumber of threads to use [0(default)-250]\n"
			"\t"OPT_SCAN_TYPE	"\t\tSYN/NULL/FIN/XMAS/ACK/UDP/CON\n"
			"\t"OPT_SLOW_MODE	"\t\t" SLOW_NINJA " | " SLOW_TIRED " | " SLOW_AWAKEN " | " SLOW_COFFEE " | " SLOW_COCAINE" \n"
			"\t"OPT_NO_VERBOSE	"\tA less verbose mode\n"
			"\t"OPT_NEURONE		"\tFind neurones instead of port\n"
		);
	}
}
