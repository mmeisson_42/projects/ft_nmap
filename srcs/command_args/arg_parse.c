
#include "ft_nmap.h"
#include "command_args.h"


/*
**	Options handled by the programm
*/

const struct s_dataoption		options[] = {
	{ . text = OPT_HELP, .callback = call_helper },
	{ . text = OPT_PORTS, .callback = set_ports },
	{ . text = OPT_IP, .callback = set_ips },
	{ . text = OPT_THREADS, .callback = set_threads },
	{ . text = OPT_SCAN_TYPE, .callback = set_scans },
	{ . text = OPT_FILE, .callback = handle_file },
	{ . text = OPT_NEURONE, .callback = get_neurone },
	{ . text = OPT_NO_VERBOSE, .callback = set_no_verbose },
	{ . text = OPT_SLOW_MODE, .callback = set_slow_mode },
};



int		arg_parse(int ac, char **av, s_options *opts)
{

	for (int i = 1; i < ac; i++)
	{
		int option_found = 0;
		int opt_error = 1;
		FOR_ARRAY (options, j)
		{
			typeof(options[j]) option = options[j];
			if (ft_strcmp(option.text, av[i]) == 0)
			{
				option_found = 1;
				if (option.callback != NULL)
				{
					opt_error = option.callback(&i, av, opts);
				}
				else
					dprintf(2, "Option %s not implemented\n", options[j].text);
				if (opt_error != 0)
					return -1;
			}
		}
		if (option_found == 0)
		{
			dprintf(2, "Error : Option %s not valid\n", av[i]);
			return -1;
		}
	}
	return 0;
}
