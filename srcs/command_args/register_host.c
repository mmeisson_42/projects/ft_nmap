
#include "command_args.h"
#include "helpers.h"

int		register_host(const char *name, struct s_list **hosts)
{
	struct s_host	h;
	struct s_list	*node;

	h.addressv4 = resolve_host(name);

	if (h.addressv4 == NULL)
	{
		return -1;
	}
	if (
			(h.name = ft_strdup(name)) == NULL
				|| (node = ft_lstnew(&h, sizeof h)) == NULL
	   )
	{
		perror("ft_nmap: ");
		return -1;
	}
	ft_lstadd(hosts, node);
	return 0;
}
