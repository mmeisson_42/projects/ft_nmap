
#include <stdlib.h>

#include "helpers.h"
#include "ft_nmap.h"

void		init_responses(s_options *opts, s_response *responses)
{
	for (size_t i = 0; i < MAX_PORT; i++)
	{
		responses[i].port = opts->ports[i];
		if (responses[i].port == NOT_A_PORT)
		{
			break ;
		}
		responses[i].scan_state[SCAN_NULL] = OPEN_FILTERED;
		responses[i].scan_state[SCAN_ACK] = FILTERED;
		responses[i].scan_state[SCAN_FIN] = OPEN_FILTERED;
		responses[i].scan_state[SCAN_SYN] = FILTERED;
		responses[i].scan_state[SCAN_UDP] = OPEN_FILTERED;
		responses[i].scan_state[SCAN_XMAS] = OPEN_FILTERED;
	}
}
