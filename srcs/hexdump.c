
#include <stdlib.h>
#include <stdio.h>

void		hexdump(void	*scrogneugneu, size_t len)
{
	unsigned char		*data = scrogneugneu;

	// BUG DO NOT HANDLE LEN NOT MULTIPLE OF 2 PROPERLY
	for (size_t i = 0; i < len; i += 2)
	{
		if (i % 16 == 0)
		{
			printf("\n%p\t", data + i);
		}
		printf(" %.2hhx%.2hhx", data[i], data[i + 1]);
	}
	printf("\n");
}
