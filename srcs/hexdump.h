#ifndef HEXDUMP
# define HEXDUMP

#include <stdlib.h>

void		hexdump(void * data, size_t len);

#endif
