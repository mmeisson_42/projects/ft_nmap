
#include <stdio.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

#include "ft_nmap.h"

#define TEST_MAX_NAME_LENGTH		32

typedef struct	s_test_set
{
	char		name[TEST_MAX_NAME_LENGTH];
	char		*(* exec)(void);
}				s_test_set;

char		*test_arg_ports(void);
char		*test_arg_ip(void);
char		*test_arg_file(void);
char		*test_arg_threads(void);
char		*test_arg_scan(void);

int main(void)
{
	s_test_set		tests[] = {
		{ .name = "test_arg_ports", .exec = test_arg_ports },
		{ .name = "test_arg_ip", .exec = test_arg_ip },
		{ .name = "test_arg_file", .exec = test_arg_file },
		{ .name = "test_arg_threads", .exec = test_arg_threads },
		{ .name = "test_arg_scan", .exec = test_arg_scan },
	};

	FOR_ARRAY (tests, i)
	{
		s_test_set	*test = tests + i;

		if (fork() == 0)
		{
			char	*res = test->exec();
			if (res == NULL)
			{
				printf("\033[032m[ %s ]\033[0m\n", test->name);
			}
			else
			{
				dprintf(2, "\033[031m[ %s ]\033[0m :: %s\n", test->name, res);
			}
			return 1;
		}
		else
		{
			int ret;
			wait(&ret);
			if (WIFSIGNALED(ret))
			{
				dprintf(
					2,
					"\033[031m[ %s ]\033[0m :: SIGNALED :: %s (%d)\n",
					test->name,
					strsignal(WTERMSIG(ret)),
					WTERMSIG(ret)
			   );
			}
		}
	}
}
