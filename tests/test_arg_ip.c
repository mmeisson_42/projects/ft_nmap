
#include <stdio.h>

#include "arg_parse.h"
#include "libft.h"

void	free_hosts(void *hosts, size_t size)
{
	if (size == sizeof(s_host))
	{
		s_host *h = hosts;
		free(h->name);
		free(h->addressv4);
		free(hosts);
	}
}

char	*test_arg_ip(void)
{
	s_options	opts;

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_IP, "127.0.0.1", NULL };
		int		ret = arg_parse(3, args, &opts);

		if (ret == -1)
		{
			return "Could not handle 127.0.0.1 without error";
		}
		if (opts.hosts == NULL)
		{
			return "Could not register node with 127.0.0.1";
		}
		s_host	*h = opts.hosts->content;
		if (ft_strcmp(h->name, "127.0.0.1") != 0)
		{
			return "hosts name not well registered ( 127.0.0.1 )";
		}
		if (ft_strcmp(h->addressv4, "127.0.0.1") != 0)
		{
			return "host's address not well registered ( 127.0.0.1 )";
		}
		ft_lstdel(&opts.hosts, free_hosts);
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_IP, "google.fr", NULL };
		int		ret = arg_parse(3, args, &opts);

		if (ret == -1)
		{
			return "Could not handle google.fr without error";
		}
		if (opts.hosts == NULL)
		{
			return "Could not register node with 127.0.0.1";
		}
		s_host	*h = opts.hosts->content;
		if (ft_strcmp(h->name, "google.fr") != 0)
		{
			return "host's name not well registered ( google.fr )";
		}
		if (ft_strcmp(h->addressv4, "216.58.204.99") != 0)
		{
			return "host's address not well registered ( google )";
		}
	}
	return NULL;
}
