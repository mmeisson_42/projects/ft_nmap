
#include "arg_parse.h"
#include "libft.h"

char		*test_arg_threads(void)
{
	s_options	opts;

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_THREADS, NULL };

		int res = arg_parse(2, args, &opts);
		if (res != -1)
		{
			return "Does not return error when nothing behind";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_THREADS, "--file", "file_name", NULL };

		int res = arg_parse(4, args, &opts);
		if (res != -1)
		{
			return "Does not return error when no arg";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_THREADS, "text", NULL };

		int res = arg_parse(3, args, &opts);
		if (res != -1)
		{
			return "Does not return an error when text as arg";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_THREADS, "-12", NULL };

		int res = arg_parse(3, args, &opts);
		if (res != -1)
		{
			return "Does not return an error when receive a too small value";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_THREADS, "256", NULL };

		int res = arg_parse(3, args, &opts);
		if (res != -1)
		{
			return "Does not return an error when receive a too high value";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_THREADS, "255", NULL };

		int res = arg_parse(3, args, &opts);
		if (res == -1)
		{
			return "Return an error when it should set 255";
		}
		if (opts.threads_number != 255)
		{
			return "Did not set threads number to required value ( 255 )";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_THREADS, "1", NULL };

		int res = arg_parse(3, args, &opts);
		if (res == -1)
		{
			return "Return an error when it should set 1";
		}
		if (opts.threads_number != 1)
		{
			return "Did not set threads number to required value ( 1 )";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_THREADS, "30", NULL };

		int res = arg_parse(3, args, &opts);
		if (res == -1)
		{
			return "Return an error when it should set 30";
		}
		if (opts.threads_number != 30)
		{
			return "Did not set threads number to required value ( 30 )";
		}
	}
	return 0;
}
