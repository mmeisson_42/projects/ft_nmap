
#include "ft_nmap.h"
#include "arg_parse.h"
#include "libft.h"

char		*test_arg_ports(void)
{
	s_options		opts;

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = {"ft_nmap", OPT_PORTS, "3", NULL};
		int ret = arg_parse(3, args, &opts);
		unsigned int expected[] = {3, NOT_A_PORT};

		if (ret == -1)
		{
			return "Unexpected error code while setting port alone";
		}
		FOR_ARRAY(expected, i)
		{
			if (expected[i] != opts.ports[i])
				return "Could not set a port alone";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = {"ft_nmap", OPT_PORTS, "3,5,6", NULL};
		int ret = arg_parse(3, args, &opts);
		unsigned int expected[] = {3, 5, 6, NOT_A_PORT};

		if (ret == -1)
		{
			return "Unexpected error code while setting several single port";
		}
		FOR_ARRAY(expected, i)
		{
			if (expected[i] != opts.ports[i])
				return "Could not set several single port";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = {"ft_nmap", OPT_PORTS, "4-9", NULL};
		int ret = arg_parse(3, args, &opts);
		unsigned int expected[] = {4, 5, 6, 7, 8, NOT_A_PORT};

		if (ret == -1)
		{
			return "Unexpected error code while setting a range of port";
		}
		FOR_ARRAY(expected, i)
		{
			if (expected[i] != opts.ports[i])
				return "Could not set a range of port";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = {"ft_nmap", OPT_PORTS, "1,2,4-9", NULL};
		int ret = arg_parse(3, args, &opts);
		unsigned int expected[] = {1, 2, 4, 5, 6, 7, 8, NOT_A_PORT};

		if (ret == -1)
		{
			return "Unexpected error code while setting mixed single/range of port";
		}
		FOR_ARRAY(expected, i)
		{
			if (expected[i] != opts.ports[i])
				return "Could not set mixed single/range of ports";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = {"ft_nmap", OPT_PORTS, "text", NULL};
		int ret = arg_parse(3, args, &opts);

		if (ret != -1)
			return "Does not return an error when text is an arg";
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = {"ft_nmap", OPT_PORTS, "a,b,5-12", NULL};
		int ret = arg_parse(3, args, &opts);

		if (ret != -1)
			return "Does not return an error when text in arg";
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = {"ft_nmap", OPT_PORTS, NULL};
		int ret = arg_parse(2, args, &opts);

		if (ret != -1)
			return "Does not return an error when does not have ports arg";
	}

	return NULL;
}
