
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

#include "ft_nmap.h"
#include "libft.h"
#include "arg_parse.h"

# define FILE_NAME "/tmp/__turlututu_tests"

void	context(char *file_content)
{
	int fd = open(FILE_NAME, O_TRUNC | O_WRONLY | O_CREAT, 0664);

	if (fd == -1)
	{
		perror("context: ");
		exit(EXIT_FAILURE);
	}
	write(fd, file_content, ft_strlen(file_content));
	close(fd);
}

char	*test_arg_file(void)
{
	s_options	opts;

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_FILE, NULL };

		int ret = arg_parse(2, args, &opts);
		if (ret != -1)
		{
			return "--file does not return error without file name";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", OPT_FILE, "bad_file_name", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret != -1)
		{
			return "--file does not return error with bad file name";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		const char		*dir_name = "/tmp/__test_dir";
		char			*args[] = { "ft_nmap", OPT_FILE, (char *)dir_name, NULL };

		if (mkdir(dir_name, 0777) == -1)
		{
			perror("mkdir: ");
			exit(1);
		}
		int ret = arg_parse(3, args, &opts);
		rmdir(dir_name);
		if (ret != -1)
		{
			return "--file does not return error with directory as file name";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char			*args[] = { "ft_nmap", OPT_FILE, FILE_NAME, NULL };

		context("");
		int ret = arg_parse(3, args, &opts);
		if (ret != -1)
		{
			return "--file does not return error with an empty file";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char			*args[] = { "ft_nmap", OPT_FILE, FILE_NAME, NULL };

		context("\n");
		int ret = arg_parse(3, args, &opts);
		if (ret != -1)
		{
			return "--file does not return error with a new line only file";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char			*args[] = { "ft_nmap", OPT_FILE, FILE_NAME, NULL };

		context("\n\n\n");
		int ret = arg_parse(3, args, &opts);
		if (ret != -1)
		{
			return "--file does not return error with a new lineS only file";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char			*args[] = { "ft_nmap", OPT_FILE, FILE_NAME, NULL };

		context("google.fr");
		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "--file return an error with a file containing google.fr";
		}
		if (opts.hosts == NULL)
		{
			return "Could not register node with google.fr";
		}
		s_host	*h = opts.hosts->content;
		if (ft_strcmp(h->name, "google.fr") != 0)
		{
			return "host's name not well registered ( google.fr )";
		}
		if (ft_strcmp(h->addressv4, "216.58.204.99") != 0)
		{
			return "host's address not well registered ( google.fr )";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char			*args[] = { "ft_nmap", OPT_FILE, FILE_NAME, NULL };

		context("127.0.0.1");
		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "--file return an error with a file containing 127.0.0.1";
		}
		if (opts.hosts == NULL)
		{
			return "Could not register node with 127.0.0.1";
		}
		s_host	*h = opts.hosts->content;
		if (ft_strcmp(h->name, "127.0.0.1") != 0)
		{
			return "host's name not well registered ( 127.0.0.1 )";
		}
		if (ft_strcmp(h->addressv4, "127.0.0.1") != 0)
		{
			return "host's address not well registered ( 127.0.0.1 )";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char			*args[] = { "ft_nmap", OPT_FILE, FILE_NAME, NULL };

		context("127.0.0.1 google.fr");
		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "--file return an error with a file containing mixed 1";
		}
		if (ft_lstlen(opts.hosts) != 2)
		{
			return "Could not register nodes with mixed 1";
		}
		const s_host expected[] = {
			{ .name = "google.fr", .addressv4 = "216.58.204.99" },
			{ .name = "127.0.0.1", .addressv4 = "127.0.0.1" },
		};

		struct s_list	*iter = opts.hosts;
		FOR_ARRAY (expected, i)
		{
			s_host		*res = iter->content;
			if (ft_strcmp(expected[i].name, res->name) | ft_strcmp(expected[i].addressv4, res->addressv4))
			{
				return "Could not register mixed 1";
			}
			iter = iter->next;
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char			*args[] = { "ft_nmap", OPT_FILE, FILE_NAME, NULL };

		context("127.0.0.1 google.fr\ngoogle.fr 127.0.0.1");
		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "--file return an error with a file containing mixed 2";
		}
		if (ft_lstlen(opts.hosts) != 4)
		{
			return "Could not register nodes with mixed 2";
		}
		const s_host expected[] = {
			{ .name = "127.0.0.1", .addressv4 = "127.0.0.1" },
			{ .name = "google.fr", .addressv4 = "216.58.204.99" },
			{ .name = "google.fr", .addressv4 = "216.58.204.99" },
			{ .name = "127.0.0.1", .addressv4 = "127.0.0.1" },
		};

		struct s_list	*iter = opts.hosts;
		FOR_ARRAY (expected, i)
		{
			s_host		*res = iter->content;
			if (ft_strcmp(expected[i].name, res->name) | ft_strcmp(expected[i].addressv4, res->addressv4))
			{
				return "Could not register mixed 2";
			}
			iter = iter->next;
		}
	}
	return NULL;
}
