
#include "ft_nmap.h"
#include "arg_parse.h"
#include "libft.h"

char		*test_arg_scan(void)
{
	s_options		opts;

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", NULL };

		int ret = arg_parse(2, args, &opts);
		if (ret != -1)
		{
			return "Does not return error when " OPT_SCAN_TYPE " not followed by arg";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "--file", "Makefile", NULL };

		int ret = arg_parse(4, args, &opts);
		if (ret != -1)
		{
			return "Does not return error when " OPT_SCAN_TYPE " followed by an other option";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret != -1)
		{
			return "Does not return error when no scan selected";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "invalid", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret != -1)
		{
			return "Does not return error when single invalid scan";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "NULL/INVALID/XMAS", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret != -1)
		{
			return "Does not return error when single invalid scan";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "XMAS", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "Return an error with XMAS scan";
		}
		if ((opts.mask_scans ^ SCAN_XMAS) != 0)
		{
			return "Did not register XMAS properly";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "SYN", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "Return an error with SYN scan";
		}
		if ((opts.mask_scans ^ SCAN_SYN) != 0)
		{
			return "Did not register SYN properly";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "NULL", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "Return an error with NULL scan";
		}
		if ((opts.mask_scans ^ SCAN_NULL) != 0)
		{
			return "Did not register NULL properly";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "FIN", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "Return an error with FIN scan";
		}
		if ((opts.mask_scans ^ SCAN_FIN) != 0)
		{
			return "Did not register FIN properly";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "ACK", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "Return an error with ACK scan";
		}
		if ((opts.mask_scans ^ SCAN_ACK) != 0)
		{
			return "Did not register ACK properly";
		}
	}

	ft_bzero(&opts, sizeof opts);
	{
		char	*args[] = { "ft_nmap", "--scan", "UDP", NULL };

		int ret = arg_parse(3, args, &opts);
		if (ret == -1)
		{
			return "Return an error with UDP scan";
		}
		if ((opts.mask_scans ^ SCAN_UDP) != 0)
		{
			return "Did not register UDP properly";
		}
	}
	return NULL;
}
