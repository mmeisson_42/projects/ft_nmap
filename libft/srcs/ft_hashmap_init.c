#include "hash_map.h"

int			ft_hashmap_init(struct s_hashmap *map)
{
	ft_bzero(map, sizeof(*map));
	map->array = ft_memalloc(sizeof(struct s_list *) * BASE_CAPACITY);
	if (map == NULL)
	{
		return (-1);
	}
	map->capacity = BASE_CAPACITY;
	return 0;
}
