/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreemap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 14:24:45 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:05:16 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_btree		*ft_btreemap(t_btree *btree, t_btree *(*f)(t_btree *elem))
{
	t_btree		*node;

	if (!btree)
		return (NULL);
	node = f(btree);
	node->left = ft_btreemap(btree->left, f);
	node->right = ft_btreemap(btree->right, f);
	return (node);
}
