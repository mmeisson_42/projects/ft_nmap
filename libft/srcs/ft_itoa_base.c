/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:58:30 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:04:22 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static int	get_size(unsigned int n, unsigned int base)
{
	int				i;

	i = 1;
	while (n > base - 1)
	{
		n /= base;
		i++;
	}
	return (i);
}

char		*ft_itoa_base(unsigned int n, unsigned int base)
{
	int				i;
	char			*tab;
	const char		characters[] = "0123456789abcdef";

	if (base > 16 || base < 2)
		return (NULL);
	i = get_size(n, base);
	if (!(tab = malloc(sizeof(char) * (i + 1))))
		return (NULL);
	tab[i--] = 0;
	while (n > base - 1)
	{
		tab[i--] = characters[n % base];
		n /= base;
	}
	tab[i--] = characters[n];
	return (tab);
}
