/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 11:01:56 by mmeisson          #+#    #+#             */
/*   Updated: 2015/12/09 16:00:06 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strchr(const char *str, int c)
{
	while (*str)
	{
		if ((int)*str == c)
			return ((char *)str);
		str++;
	}
	if (c == 0)
	{
		return ((char *)str);
	}
	return (NULL);
}
