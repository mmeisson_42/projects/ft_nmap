#include "hash_map.h"

void	*ft_hashmap_get(struct s_hashmap *map, const void *key, size_t key_size)
{
	size_t			hash;
	struct s_list	*iter;
	struct s_node	*node;

	hash = ft_hashmap_hash(key, key_size);
	iter = map->array[hash % map->capacity];
	while (iter != NULL)
	{
		node = iter->content;
		if (key_size == node->key_size && ft_memcmp(key, node->key, key_size) == 0)
		{
			return (node->content);
		}
		iter = iter->next;
	}
	return (NULL);
}
