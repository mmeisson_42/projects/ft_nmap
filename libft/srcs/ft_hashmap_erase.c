#include <stdlib.h>
#include "hash_map.h"
#include "libft.h"

void		ft_hashmap_erase(struct s_hashmap *map, void (*callback)(void *, size_t))
{
	size_t			i;

	i = 0;
	while (i < map->capacity)
	{
		ft_lstdel(map->array + i, callback);
		i++;
	}
	free(map->array);
	ft_bzero(map, sizeof(*map));
}
