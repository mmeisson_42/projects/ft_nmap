#include "hash_map.h"

int		ft_hashmap_shrink(struct s_hashmap *map)
{
	size_t			i;
	size_t			new_capacity;
	struct s_list	**grew;
	struct s_list	*elem;
	struct s_node	*node;

	i = 0;
	new_capacity = map->capacity >> 1;
	if (new_capacity < BASE_CAPACITY)
	{
		return (0);
	}
	grew = malloc(sizeof(struct s_list *) * (new_capacity));
	if (grew == NULL)
	{
		return (-1);
	}
	while (i < map->capacity)
	{
		while ((elem = ft_lstpop(map->array + i)) != NULL)
		{
			node = elem->content;
			ft_lstadd(grew + (node->real_hash % new_capacity), elem);
		}
		i++;
	}
	map->capacity = new_capacity;
	return (0);
}
