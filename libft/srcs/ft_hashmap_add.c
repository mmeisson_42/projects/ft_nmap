#include <stdlib.h>

#include "hash_map.h"
#include "libft.h"

struct s_node		*allocate_node(const void *key, size_t key_size, const void *content, size_t content_size, size_t *full_size)
{
	void				*allocation;
	struct s_node		*node;
	size_t				key_index;
	size_t				node_index;

	key_index = content_size;
	if (key_index % 16 != 0)
		key_index = (key_index / 16) * 16 + 16;

	node_index = key_index + key_size;
	if (node_index % 16 != 0)
		node_index = (node_index / 16) * 16 + 16;

	*full_size = node_index + sizeof(*node);
	allocation = malloc(*full_size);
	if (allocation == NULL)
	{
		return (NULL);
	}

	node = allocation + node_index;

	node->content = allocation;
	ft_memcpy(node->content, content, content_size);
	node->content_size = content_size;

	node->key = allocation + key_index;
	ft_memcpy(node->key, key, key_size);
	node->key_size = key_size;

	node->real_hash = ft_hashmap_hash(key, key_size);
	return (node);
}

int		ft_hashmap_add(
	struct s_hashmap *map,
	const void *key,
	size_t key_size,
	const void *content,
	size_t content_size
)
{
	struct s_list		*list;
	struct s_node		*node;
	size_t				full_size;

	node = allocate_node(key, key_size, content, content_size, &full_size);
	if (node == NULL)
	{
		return (-1);
	}
	list = ft_lstnew_cpy(node, full_size);
	if (list == NULL)
	{
		free(node->content); // This free node too
		return (-1);
	}
	ft_lstadd(map->array + (node->real_hash % map->capacity), list);
	map->size++;
	if (((100 * map->size) / map->capacity) > MAX_COMPLETION_RATE)
	{
		return (ft_hashmap_grow(map));
	}
	return (0);
}
