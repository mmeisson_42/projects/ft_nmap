/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_hexa.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:56:42 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/11 11:18:36 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int		ft_atoi_hexa(const char *str)
{
	unsigned long		nb;

	while (ft_isspace(*str))
		str++;
	nb = 0;
	str += (*str == '+' || *str == '-') ? 1 : 0;
	while (*str)
	{
		if (ft_isdigit(*str))
			nb = nb * 10 + (*str - 48);
		else if (*str >= 'A' && *str <= 'F')
			nb = nb * 10 + ((*str - 'A') + 10);
		else if (*str >= 'a' && *str <= 'f')
			nb = nb * 10 + ((*str - 'a') + 10);
		else
			break ;
		str++;
	}
	return ((unsigned int)nb);
}
