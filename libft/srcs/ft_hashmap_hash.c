#include <stdlib.h>
#include "hash_map.h"
#include <stdint.h>

static size_t		remaining_bytes(unsigned char *remain, size_t bytes)
{
	size_t			ret;
	size_t			i;

	ret = 0;
	i = 0;
	while (i < bytes)
	{
		ret = ret * MULTIPLIER + (size_t)(remain[i]);
		i++;
	}
	return (ret);
}

size_t		ft_hashmap_hash(const void * key, size_t key_size)
{
	size_t			ret;
	size_t			i;
	size_t			max_i;
	uint32_t		*ukey;

	ret = 0;
	i = 0;
	max_i = key_size / sizeof(uint32_t);
	ukey = (uint32_t *)key;
	while (i < max_i)
	{
		ret = ret * MULTIPLIER + *ukey;
		ukey++;
		i += sizeof(uint32_t);
	}
	ret = (ret + remaining_bytes((unsigned char *)ukey, key_size % sizeof(uint32_t)));
	return (ret);
}
