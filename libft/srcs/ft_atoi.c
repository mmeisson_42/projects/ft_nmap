/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 10:56:42 by mmeisson          #+#    #+#             */
/*   Updated: 2018/12/11 11:18:36 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	unsigned long		nb;
	int					neg;

	while (ft_isspace(*str))
		str++;
	nb = 0;
	neg = (*str == '-') ? 1 : 0;
	str += (*str == '+' || *str == '-') ? 1 : 0;
	while (*str && ft_isdigit(*str))
	{
		nb = nb * 10 + (*str - 48);
		str++;
	}
	return (neg) ? ((int)-nb) : ((int)nb);
}
