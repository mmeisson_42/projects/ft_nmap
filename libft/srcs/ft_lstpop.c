#include "libft.h"

struct s_list		*ft_lstpop(struct s_list **head)
{
	struct s_list	*node;

	if (*head == NULL)
	{
		return (NULL);
	}
	node = *head;
	*head = node->next;
	node->next = NULL;
	return (node);
}
