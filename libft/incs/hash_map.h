#ifndef HASH_MAP
# define HASH_MAP

# define MULTIPLIER				37
# define BASE_CAPACITY			(1 << 4)
# define MAX_COMPLETION_RATE	70
# define MIN_COMPLETION_RATE	10

# include <stdlib.h>
# include "libft.h"

typedef struct		s_node
{
	void		*key;
	size_t		key_size;
	void		*content;
	size_t		content_size;
	size_t		real_hash;
}					t_node;

typedef struct		s_hashmap
{
	size_t			size;
	size_t			capacity;
	struct s_list	**array;
}					t_hashmap;

size_t		ft_hashmap_hash(const void * key, size_t key_size);
int			ft_hashmap_grow(struct s_hashmap *map);
int			ft_hashmap_shring(struct s_hashmap *map);
int			ft_hashmap_init(struct s_hashmap *map);
int			ft_hashmap_add(struct s_hashmap *map, const void *key, size_t key_size, const void *content, size_t content_size);
void		ft_hashmap_erase(struct s_hashmap *map, void (*callback)(void *, size_t));
void		*ft_hashmap_get(struct s_hashmap *map, const void * key, size_t key_size);

#endif
