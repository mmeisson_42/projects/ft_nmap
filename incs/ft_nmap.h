#ifndef FT_NMAP_H
# define FT_NMAP_H

# include <netinet/ip.h>
# include <netinet/ip_icmp.h>
# include <netinet/tcp.h>
# include <netinet/udp.h>
# include "libft.h"

# define DEFAULT_TTL			64
# define SERVICE_LEN			36
# define DEFAULT_THREADS		0

# define UNASSIGNED				"Unassigned"

# define REQ_SCAN_NULL	(1 << 0)
# define REQ_SCAN_ACK	(1 << 1)
# define REQ_SCAN_FIN	(1 << 2)
# define REQ_SCAN_SYN	(1 << 3)
# define REQ_SCAN_UDP	(1 << 4)
# define REQ_SCAN_XMAS	(1 << 5)
# define REQ_SCAN_CON	(1 << 6)
# define REQ_SCAN_ALL (REQ_SCAN_NULL | REQ_SCAN_ACK | REQ_SCAN_SYN | REQ_SCAN_FIN | REQ_SCAN_UDP | REQ_SCAN_XMAS | REQ_SCAN_CON)
# define REQ_SCAN_ROOT (REQ_SCAN_NULL | REQ_SCAN_ACK | REQ_SCAN_SYN | REQ_SCAN_FIN | REQ_SCAN_UDP | REQ_SCAN_XMAS)

# define SCAN_NULL_STR		"NULL"
# define SCAN_ACK_STR		"ACK"
# define SCAN_FIN_STR		"FIN"
# define SCAN_SYN_STR		"SYN"
# define SCAN_UDP_STR		"UDP"
# define SCAN_XMAS_STR		"XMAS"
# define SCAN_CON_STR		"CON"

# define SCAN_NULL 0
# define SCAN_ACK 1
# define SCAN_FIN 2
# define SCAN_SYN 3
# define SCAN_UDP 4
# define SCAN_XMAS 5
# define SCAN_CON 6
# define SCAN_ERR -1

# define SCAN_TYPES 7

# define ARRAY_LEN(array)		(sizeof array / sizeof array[0])

# define FOR_ARRAY(array, iterator) for ( \
	size_t iterator = 0; \
	iterator < ARRAY_LEN(array); \
	iterator++)

typedef enum		e_service_state
{
# define OPEN_STR		"Open"
	OPEN,
# define UNFILTERED_STR	"Unfiltered"
	UNFILTERED,
# define FILTERED_STR	"Filtered"
	FILTERED,
# define OPEN_FILTERED_STR	"Open|Filtered"
	OPEN_FILTERED,
# define CLOSE_STR		"Close"
	CLOSE,
}					e_service_state;

typedef struct		s_response
{
	unsigned int	port;
	unsigned char	service[SERVICE_LEN];
	e_service_state	scan_state[SCAN_TYPES];
}					s_response;


/*
**	Virtual structure for building raw dgram
*/
typedef struct		s_raw
{
	struct iphdr	layer3;
	unsigned char	layer4[1];
}					s_raw;

typedef struct		s_tcp
{
	struct iphdr	layer3;
	struct tcphdr	layer4;
	unsigned char	options[4];
}					s_tcp;

typedef struct		s_udp
{
	struct iphdr	layer3;
	struct udphdr	layer4;
}					s_udp;

typedef struct		s_icmp
{
	struct iphdr	layer3;
	struct icmphdr	layer4;
}					s_icmp;

typedef struct	port_table
{
	int			sockets[SCAN_TYPES];
	int			ports[SCAN_TYPES];
}				s_port_table;


extern s_port_table		g_port_table;

#endif
